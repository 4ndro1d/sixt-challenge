
# Sixt Code Challenge

The application fetches JSON data from https://cdn.sixt.io/codingtask/cars and
displays it on a map and in a list

## Architecture

The app is build using clean architecture and MVVM

#### Remote

Includes call to API using Retrofit

#### Data

In this example the data layer is just a proxy to remote. 
Usually caching would be implemented here

#### Domain

Includes use cases, which fetch data from data repository

#### ViewModel

Intermediator between business logic and view. Prepares data to be viewed and
might validate user input

#### View

Represented by Android UI components like activities and fragments

##### Notes

Usually we have mapping of models of each layer between all layers. For the sake
of simplicity of this sample we only map from remote to domain and use domain
models in the view as well

### Tests

For now unit tests for mappers were implemented. Unit tests are missing for 
viewmodels, repository and use cases. Also Espresso tests to test the UI could
be added

### Libraries

Name| Usage
------------ | -------------
Koin | Dependency Injection
RxJava | Threading
Retrofit & OkHttp | Networking
Moshi | JSON Adapter
Glide | Image loading
Mockito | Unit tests
Jetpack | Architecture and UI components
