package com.droidapps.sixtchallenge.common.domain

interface NoArgsUseCase<RETURN> {

    fun execute(): RETURN
}