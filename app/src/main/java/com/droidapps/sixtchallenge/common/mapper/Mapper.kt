package com.droidapps.sixtchallenge.common.mapper

interface Mapper<FROM, TO> {

    fun map(from: FROM): TO
}