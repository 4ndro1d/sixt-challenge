package com.droidapps.sixtchallenge.common.domain

interface UseCase<PARAMS, RETURN> {

    fun execute(params: PARAMS): RETURN
}