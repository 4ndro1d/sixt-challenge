package com.droidapps.sixtchallenge

import android.app.Application
import com.droidapps.sixtchallenge.di.appModule
import org.koin.core.context.startKoin
import timber.log.Timber

class MainApp : Application() {

    override fun onCreate() {
        super.onCreate()

        Timber.plant(Timber.DebugTree())

        startKoin {
            modules(
                listOf(appModule)
            )
        }
    }
}