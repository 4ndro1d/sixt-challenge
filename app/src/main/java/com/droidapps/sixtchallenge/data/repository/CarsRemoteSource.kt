package com.droidapps.sixtchallenge.data.repository

import com.droidapps.sixtchallenge.domain.model.Car
import io.reactivex.Single

interface CarsRemoteSource {

    fun loadCars(): Single<List<Car>>
}
