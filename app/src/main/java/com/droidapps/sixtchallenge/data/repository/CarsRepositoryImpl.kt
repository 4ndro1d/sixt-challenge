package com.droidapps.sixtchallenge.data.repository

import com.droidapps.sixtchallenge.domain.model.Car
import com.droidapps.sixtchallenge.domain.repository.CarsRepository
import io.reactivex.Single

class CarsRepositoryImpl(
    private val remote: CarsRemoteSource
) : CarsRepository {

    override fun loadCars(): Single<List<Car>> =
        remote.loadCars()
}