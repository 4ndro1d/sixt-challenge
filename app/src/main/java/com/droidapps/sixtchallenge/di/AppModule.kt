package com.droidapps.sixtchallenge.di

import com.droidapps.sixtchallenge.data.repository.CarsRemoteSource
import com.droidapps.sixtchallenge.data.repository.CarsRepositoryImpl
import com.droidapps.sixtchallenge.domain.repository.CarsRepository
import com.droidapps.sixtchallenge.domain.usecase.LoadCarsUseCase
import com.droidapps.sixtchallenge.remote.api.CarsApi
import com.droidapps.sixtchallenge.remote.api.SixtRetrofit
import com.droidapps.sixtchallenge.remote.mapper.FuelTypeMapper
import com.droidapps.sixtchallenge.remote.mapper.RemoteCarMapper
import com.droidapps.sixtchallenge.remote.mapper.TransmissionTypeMapper
import com.droidapps.sixtchallenge.remote.repository.CarsRemoteSourceImpl
import com.droidapps.sixtchallenge.viewmodel.CarsViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {

    single<CarsApi> { SixtRetrofit.retrofit.create(CarsApi::class.java) }
    single { FuelTypeMapper() }
    single { TransmissionTypeMapper() }
    single { RemoteCarMapper(fuelTypeMapper = get(), transmissionTypeMapper = get()) }
    single<CarsRemoteSource> { CarsRemoteSourceImpl(api = get(), carMapper = get()) }
    single<CarsRepository> { CarsRepositoryImpl(remote = get()) }
    single { LoadCarsUseCase(repository = get()) }
    viewModel { CarsViewModel(loadCarsUseCase = get()) }
}