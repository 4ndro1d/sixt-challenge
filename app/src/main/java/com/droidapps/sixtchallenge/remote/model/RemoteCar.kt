package com.droidapps.sixtchallenge.remote.model

import com.squareup.moshi.Json

data class RemoteCar(
    @Json(name = "id") val id: String,
    @Json(name = "longitude") val longitude: Double,
    @Json(name = "latitude") val latitude: Double,
    @Json(name = "name") val name: String,
    @Json(name = "model") val modelName: String,
    @Json(name = "carImageUrl") val carImageUrl: String,
    @Json(name = "license") val licensePlate: String,
    @Json(name = "transmission") val transmission: String,
    @Json(name = "fuelLevel") val fuelLevel: Double,
    @Json(name = "fuelType") val fuelType: String
)