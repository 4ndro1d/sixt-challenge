package com.droidapps.sixtchallenge.remote.mapper

import com.droidapps.sixtchallenge.common.mapper.Mapper

class FuelTypeMapper : Mapper<String, String> {

    override fun map(from: String): String = when (from) {
        "E" -> "Electro"
        "P" -> "Petrol"
        "D" -> "Diesel"
        else -> ""
    }
}
