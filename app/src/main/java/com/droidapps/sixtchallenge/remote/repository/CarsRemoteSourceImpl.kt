package com.droidapps.sixtchallenge.remote.repository

import com.droidapps.sixtchallenge.data.repository.CarsRemoteSource
import com.droidapps.sixtchallenge.domain.model.Car
import com.droidapps.sixtchallenge.remote.api.CarsApi
import com.droidapps.sixtchallenge.remote.mapper.RemoteCarMapper
import io.reactivex.Single

class CarsRemoteSourceImpl(
    private val api: CarsApi,
    private val carMapper: RemoteCarMapper
) : CarsRemoteSource {

    override fun loadCars(): Single<List<Car>> =
        api.loadCars().map {
            it.map(carMapper::map)
        }
}