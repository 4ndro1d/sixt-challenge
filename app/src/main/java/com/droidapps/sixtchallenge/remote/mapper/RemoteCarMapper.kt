package com.droidapps.sixtchallenge.remote.mapper

import com.droidapps.sixtchallenge.common.mapper.Mapper
import com.droidapps.sixtchallenge.domain.model.Car
import com.droidapps.sixtchallenge.remote.model.RemoteCar

class RemoteCarMapper(
    private val fuelTypeMapper: FuelTypeMapper,
    private val transmissionTypeMapper: TransmissionTypeMapper
) : Mapper<RemoteCar, Car> {

    override fun map(from: RemoteCar): Car = with(from) {
        Car(
            id = id,
            long = longitude,
            lat = latitude,
            name = name,
            modelName = modelName,
            imageUrl = carImageUrl,
            licensePlate = licensePlate,
            transmission = transmissionTypeMapper.map(transmission),
            fuelLevel = (fuelLevel * 100).toInt(),
            fuel = fuelTypeMapper.map(fuelType)
        )
    }
}
