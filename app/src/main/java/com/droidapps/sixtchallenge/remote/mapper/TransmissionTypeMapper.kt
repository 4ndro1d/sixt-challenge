package com.droidapps.sixtchallenge.remote.mapper

import com.droidapps.sixtchallenge.common.mapper.Mapper

class TransmissionTypeMapper : Mapper<String, String> {

    override fun map(from: String): String = when (from) {
        "M" -> "Manual"
        "A" -> "Automatic"
        else -> ""
    }
}