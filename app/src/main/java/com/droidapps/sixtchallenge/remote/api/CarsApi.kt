package com.droidapps.sixtchallenge.remote.api

import com.droidapps.sixtchallenge.remote.model.RemoteCar
import io.reactivex.Single
import retrofit2.http.GET

interface CarsApi {

    @GET("cars")
    fun loadCars(): Single<List<RemoteCar>>
}
