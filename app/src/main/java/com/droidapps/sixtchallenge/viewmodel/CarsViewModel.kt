package com.droidapps.sixtchallenge.viewmodel

import androidx.lifecycle.MutableLiveData
import com.droidapps.sixtchallenge.common.extensions.observeOnMain
import com.droidapps.sixtchallenge.common.extensions.subscribeOnIo
import com.droidapps.sixtchallenge.common.viewmodel.RxViewModel
import com.droidapps.sixtchallenge.domain.model.Car
import com.droidapps.sixtchallenge.domain.usecase.LoadCarsUseCase
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import timber.log.Timber

class CarsViewModel(
    private val loadCarsUseCase: LoadCarsUseCase
) : RxViewModel() {

    val carsLiveData: MutableLiveData<List<Car>> by lazy {
        MutableLiveData<List<Car>>()
    }

    val errorLiveData: MutableLiveData<String> by lazy {
        MutableLiveData<String>()
    }

    fun loadCars() {
        disposables += loadCarsUseCase.execute()
            .subscribeOnIo()
            .observeOnMain()
            .subscribeBy(
                onSuccess = { carsLiveData.value = it },
                onError = {
                    Timber.e(it)
                    errorLiveData.value = it.message
                }
            )
    }
}