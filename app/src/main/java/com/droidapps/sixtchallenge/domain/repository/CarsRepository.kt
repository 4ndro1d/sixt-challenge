package com.droidapps.sixtchallenge.domain.repository

import com.droidapps.sixtchallenge.domain.model.Car
import io.reactivex.Single

interface CarsRepository {

    fun loadCars(): Single<List<Car>>
}
