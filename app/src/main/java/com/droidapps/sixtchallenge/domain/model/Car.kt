package com.droidapps.sixtchallenge.domain.model

data class Car(
    val id: String,
    val long: Double,
    val lat: Double,
    val name: String,
    val modelName: String,
    val imageUrl: String,
    val licensePlate: String,
    val transmission: String,
    val fuelLevel: Int,
    val fuel: String
)