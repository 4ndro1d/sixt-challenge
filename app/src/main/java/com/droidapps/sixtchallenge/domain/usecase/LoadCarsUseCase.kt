package com.droidapps.sixtchallenge.domain.usecase

import com.droidapps.sixtchallenge.common.domain.NoArgsUseCase
import com.droidapps.sixtchallenge.domain.model.Car
import com.droidapps.sixtchallenge.domain.repository.CarsRepository
import io.reactivex.Single

class LoadCarsUseCase(
    private val repository: CarsRepository
) : NoArgsUseCase<Single<List<Car>>> {

    override fun execute(): Single<List<Car>> =
        repository.loadCars()
}