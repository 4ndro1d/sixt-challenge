package com.droidapps.sixtchallenge.view

import android.annotation.SuppressLint
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.droidapps.sixtchallenge.R
import com.droidapps.sixtchallenge.common.extensions.inflate
import com.droidapps.sixtchallenge.domain.model.Car

class CarAdapter(
    var cars: List<Car>
) : RecyclerView.Adapter<CarAdapter.CarViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarViewHolder =
        CarViewHolder(parent.inflate(R.layout.car_item))

    override fun getItemCount(): Int =
        cars.size

    override fun onBindViewHolder(holder: CarViewHolder, position: Int) {
        holder.bind(cars[position])
    }

    class CarViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val nameText by lazy { itemView.findViewById<TextView>(R.id.nameText) }
        private val modelText by lazy { itemView.findViewById<TextView>(R.id.modelText) }
        private val fuelTypeText by lazy { itemView.findViewById<TextView>(R.id.fuelTypeText) }
        private val transmissionText by lazy { itemView.findViewById<TextView>(R.id.transmissionText) }
        private val fuelText by lazy { itemView.findViewById<TextView>(R.id.fuelText) }
        private val licenseText by lazy { itemView.findViewById<TextView>(R.id.licenseText) }
        private val carImage by lazy { itemView.findViewById<ImageView>(R.id.carImage) }

        @SuppressLint("SetTextI18n")
        fun bind(car: Car) {
            nameText.text = car.name
            modelText.text = car.modelName
            fuelText.text = "${car.fuelLevel} %"
            licenseText.text = car.licensePlate
            transmissionText.text = car.transmission
            fuelTypeText.text = car.fuel

            Glide.with(itemView.context)
                .load(car.imageUrl)
                .placeholder(R.drawable.car)
                .error(R.drawable.car)
                .into(carImage)
        }
    }
}
