package com.droidapps.sixtchallenge.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.droidapps.sixtchallenge.R
import com.droidapps.sixtchallenge.domain.model.Car
import com.droidapps.sixtchallenge.viewmodel.CarsViewModel
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.fragment_map.*
import org.koin.android.ext.android.inject

class MapFragment : BottomSheetDialogFragment() {

    private val carsViewModel: CarsViewModel by inject()

    private val adapter = CarAdapter(emptyList())

    private lateinit var map: GoogleMap

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(R.layout.fragment_map, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(::onMapReady)

        carList.layoutManager = LinearLayoutManager(context)
        carList.adapter = adapter

        observeErrors()
        observeCarsData()
    }

    private fun onMapReady(map: GoogleMap) {
        // load the cars data once the map is read
        // we could already display the list
        reloadCars()
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(LatLng(48.124189, 11.565600), 12.0f))
        this.map = map
    }

    private fun observeErrors() {
        carsViewModel.errorLiveData.observe(this, Observer<String> {
            showErrorState(it)
        })
    }

    private fun showErrorState(errorMessage: String) {
        Snackbar.make(mapView, errorMessage, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.retry) { reloadCars() }
            .show()
    }

    private fun observeCarsData() {
        carsViewModel.carsLiveData.observe(this, Observer<List<Car>> { cars ->
            updateCarsList(cars)
            updateMap(cars)
        })
    }

    private fun updateCarsList(cars: List<Car>) {
        adapter.cars = cars
        adapter.notifyDataSetChanged()
    }

    private fun updateMap(cars: List<Car>) {
        if (::map.isInitialized) {
            cars.forEach { car ->
                map.addMarker(MarkerOptions().apply {
                    position(LatLng(car.lat, car.long))
                })
            }
        }
    }

    private fun reloadCars() {
        carsViewModel.loadCars()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }
}