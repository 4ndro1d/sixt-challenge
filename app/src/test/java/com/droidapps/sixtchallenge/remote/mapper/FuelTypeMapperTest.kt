package com.droidapps.sixtchallenge.remote.mapper

import io.kotlintest.matchers.shouldBe
import org.junit.Before
import org.junit.Test

class FuelTypeMapperTest {

    private lateinit var mapper: FuelTypeMapper

    @Before
    fun setUp() {
        mapper = FuelTypeMapper()
    }

    @Test
    fun `map from D should be Diesel`() {
        mapper.map("D") shouldBe "Diesel"
    }

    @Test
    fun `map from P should be Petrol`() {
        mapper.map("P") shouldBe "Petrol"
    }

    @Test
    fun `map from E should be Electro`() {
        mapper.map("E") shouldBe "Electro"
    }

    @Test
    fun `map from invalid String should be empty String`() {
        mapper.map("") shouldBe ""
        mapper.map("foo") shouldBe ""
        mapper.map("e") shouldBe ""
    }
}