package com.droidapps.sixtchallenge.remote.mapper

import io.kotlintest.matchers.shouldBe
import org.junit.Before
import org.junit.Test

class TransmissionTypeMapperTest {

    private lateinit var mapper: TransmissionTypeMapper

    @Before
    fun setUp() {
        mapper = TransmissionTypeMapper()
    }

    @Test
    fun `map from M should be Manual`() {
        mapper.map("M") shouldBe "Manual"
    }

    @Test
    fun `map from A should be Automatic`() {
        mapper.map("A") shouldBe "Automatic"
    }

    @Test
    fun `map from invalid String should be empty String`() {
        mapper.map("") shouldBe ""
        mapper.map("foo") shouldBe ""
        mapper.map("m") shouldBe ""
    }
}