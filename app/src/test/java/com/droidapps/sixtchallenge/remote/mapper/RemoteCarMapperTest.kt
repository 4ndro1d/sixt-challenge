package com.droidapps.sixtchallenge.remote.mapper

import com.droidapps.sixtchallenge.remote.model.RemoteCar
import com.nhaarman.mockito_kotlin.any
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import io.kotlintest.matchers.exactly
import io.kotlintest.matchers.shouldBe
import org.junit.Before
import org.junit.Test

class RemoteCarMapperTest {

    private val fuelMapper: FuelTypeMapper = mock()
    private val transmissionMapper: TransmissionTypeMapper = mock()

    private lateinit var mapper: RemoteCarMapper

    @Before
    fun setUp() {
        mapper = RemoteCarMapper(
            fuelTypeMapper = fuelMapper,
            transmissionTypeMapper = transmissionMapper
        )
    }

    @Test
    fun map() {
        whenever(fuelMapper.map(any())).thenReturn("foo")
        whenever(transmissionMapper.map(any())).thenReturn("bar")

        val remote: RemoteCar = mock()
        with(remote) {
            whenever(id).thenReturn("id")
            whenever(latitude).thenReturn(0.0)
            whenever(longitude).thenReturn(1.0)
            whenever(name).thenReturn("name")
            whenever(modelName).thenReturn("model")
            whenever(carImageUrl).thenReturn("image.url")
            whenever(licensePlate).thenReturn("license")
            whenever(transmission).thenReturn("transmission")
            whenever(fuelLevel).thenReturn(0.6)
            whenever(fuelType).thenReturn("fuelType")
        }

        with(mapper.map(remote)) {
            id shouldBe "id"
            lat shouldBe 0.0
            long shouldBe 1.0
            name shouldBe "name"
            modelName shouldBe "model"
            imageUrl shouldBe "image.url"
            licensePlate shouldBe "license"
            transmission shouldBe "bar"
            fuelLevel shouldBe 60
            fuel shouldBe "foo"
        }
    }
}